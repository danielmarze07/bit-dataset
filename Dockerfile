# Trabajo Practico Semana 2
## Por lo pronto abrir el *xlsx y guardar en la variable tu_df para trabajar todo aprendido
## También guarda en otro archivo *xlsx lo que esta en el DataFrame tu_df
## Mostrar dataFrame algo anda mal
## Favor darle una revisada

from tkinter import *
import tkinter as tk
from tkinter import ttk
import pandas as pd
from tkinter import filedialog
from tkinter import messagebox, ttk
from functools import partial

tu_df=[]
tu_archivo =""
def donothing():
   filewin = Toplevel(root)
   button = Button(filewin, text="Aun en desarrollo")
   button.pack()

def buscar_archivo():
    filename = filedialog.askopenfilename(initialdir = "/",
                                          title = "Seleccione archivo",
                                          filetypes = (("Archivos Excel",
                                                        "*.xlsx*"),
                                                       ("Todos los Archivos",
                                                        "*.*")))
      
    # Change label contents
    global tu_archivo
    global tu_df
    tu_archivo = str(filename)
    tu_df = pd.read_excel(tu_archivo)#cargando al dataframe para trabajar
    mitexto = Label(None,text="ARCHIVO: "+filename, fg="firebrick")
    mitexto.place(x=50,y=30)
    
def guardararchivo():
    filename = filedialog.asksaveasfilename(initialdir = "/",
                                                 title="Guardar Archivo", defaultextension=".xlsx",
                                                 filetypes=(("Archivo Excel","*.xlsx"),
                                                             ("Archivo CSV","*.csv")))
    global tu_df
    print(f"archivo a guardad: {filename}")
    tu_df.to_excel(filename)
          
def mostrar_df():       
    root = tk.Tk()
    global tu_df
    display(tu_df)   
    cols = list(tu_df.columns)
    tree = ttk.Treeview(root)
    tree.pack()
    tree["columns"] = cols
    for i in cols:
        tree.column(i, anchor="w")
        tree.heading(i, text=i, anchor='w')
    for index, row in df.iterrows():
        tree.insert("",0,text=index,values=list(row))
    root.mainloop()
    
def ayuda():
    messagebox.showinfo(message="WCS", title="Ayuda")
    
    
root = Tk()
root.config(width=1000, height=600)
root.title("MI TRABAJO")
#########menu
menubar = Menu(root)
filemenu = Menu(menubar, tearoff = 0)
filemenu.add_command(label="Abrir BD", command = buscar_archivo)
filemenu.add_command(label = "Guardar BD", command = guardararchivo)
filemenu.add_separator()
filemenu.add_command(label = "Salir", command = root.destroy)
menubar.add_cascade(label = "Archivo", menu = filemenu)
editmenu = Menu(menubar, tearoff=0)
editmenu.add_command(label = "Eliminar Columna", command = donothing)
editmenu.add_command(label = "Eliminar Fila", command = donothing)
editmenu.add_command(label = "Ordenar Por columna", command = donothing)
editmenu.add_command(label = "Dividir columna por", command = donothing)
editmenu.add_command(label = "Tipo de Dato por columna", command = donothing)
editmenu.add_command(label = "Unir DataFrame", command = donothing)
editmenu.add_command(label = "Mostrar DataFrame", command = mostrar_df)
menubar.add_cascade(label = "Opciones", menu = editmenu)
helpmenu = Menu(menubar, tearoff=0)
helpmenu.add_command(label = "Ayuda de..?", command = ayuda)
menubar.add_cascade(label = "Ayuda", menu = helpmenu)

root.config(menu = menubar)
root.mainloop()

#display(tu_df)
